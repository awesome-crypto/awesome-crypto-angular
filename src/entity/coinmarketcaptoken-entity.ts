import {JsonObject, JsonProperty} from "json2typescript";

/**
    {
        "id": "bitcoin",
        "name": "Bitcoin",
        "symbol": "BTC",
        "rank": "1",
        "price_usd": "6752.64",
        "price_btc": "1.0",
        "24h_volume_usd": "3658070000.0",
        "market_cap_usd": "115511922584",
        "available_supply": "17106187.0",
        "total_supply": "17106187.0",
        "max_supply": "21000000.0",
        "percent_change_1h": "-0.29",
        "percent_change_24h": "1.35",
        "percent_change_7d": "3.7",
        "last_updated": "1529569777"
    }
*/
@JsonObject
export class CoinMarketCapTokenEntity {

    @JsonProperty("id", String)
    id: string = undefined;

    @JsonProperty("name", String)
    name: string = undefined;

    @JsonProperty("symbol", String)
    symbol: string = undefined;

    @JsonProperty("rank", Number)
    rank: number = undefined;

    @JsonProperty("price_usd", Number)
    priceUsd: number = undefined;

    @JsonProperty("price_btc", Number)
    priceBtc: number = undefined;

    @JsonProperty("24h_volume_usd", Number)
    volume24H: number = undefined;

    @JsonProperty("market_cap_usd", Number)
    marketCapUSD: number = undefined;

    @JsonProperty("available_supply", Number)
    availableSupply: number = undefined;

    @JsonProperty("total_supply", Number)
    totalSupply: number = undefined;

    @JsonProperty("max_supply", Number)
    maxSupply: number = undefined;

    @JsonProperty("percent_change_1h", Number)
    priceChange1H: number = undefined;

    @JsonProperty("percent_change_24h", Number)
    priceChange24H: number = undefined;

    @JsonProperty("percent_change_7d", Number)
    priceChange7d: number = undefined;

    @JsonProperty("last_updated", Number)
    lastUpdated: number = undefined;
}
