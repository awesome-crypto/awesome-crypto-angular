import { Component, OnInit, Inject } from "@angular/core";
import { MarketDataService } from "./market-data.service";
import {JsonConvert, OperationMode, ValueCheckingMode} from "json2typescript";
import { CoinMarketCapTokenEntity } from "../entity/coinmarketcaptoken-entity";

@Component({
    selector: "app-market-data",
    templateUrl: "./market-data.component.html",
    styleUrls: ["./market-data.component.css"]
})
export class MarketDataComponent implements OnInit {
    private _marketDataService: MarketDataService;
    private tokens: CoinMarketCapTokenEntity[];

    constructor(marketDataService: MarketDataService) {
        this._marketDataService = marketDataService;
    }

    ngOnInit() {
        this._marketDataService.getCoinMarketCapTokens().subscribe((res: any) => {
            let jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.operationMode = OperationMode.ENABLE;
            jsonConvert.ignorePrimitiveChecks = true;
            jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;
            try {
                this.tokens = jsonConvert.deserializeArray(res, CoinMarketCapTokenEntity);
            } catch(err) {
                console.log((<Error>err));
            }
        });
    }
}
