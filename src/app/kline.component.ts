import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MarketDataService } from "./market-data.service";

@Component({
    selector: "app-kline",
    templateUrl: "./kline.component.html"
})
export class KLineComponent implements OnInit, OnDestroy {
    private marketDataService: MarketDataService;
    private route: ActivatedRoute;
    private router: Router;
    private sub: any;

    // bind symbol here
    currentSymbol: string;

    constructor(marketDataService: MarketDataService,
                route: ActivatedRoute,
                router: Router) {
        this.marketDataService = marketDataService;
        this.route = route;
        this.router = router;
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            const symbol = params["market"];
            this.currentSymbol = this.marketDataService.getToken(symbol);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    goToMarketDataList() {
        const link = ["/"];
        this.router.navigate(link);
    }
}
