import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { CoinMarketCapTokenEntity } from "../entity/coinmarketcaptoken-entity";

const coinMarketcapCoinsUrl = "https://api.coinmarketcap.com/v1/ticker/?limit=100";

@Injectable()
export class MarketDataService {
    private _http: HttpClient;

    constructor(http: HttpClient) {
        this._http = http;
    }

    getCoinMarketCapTokens() {
        return this._http.get(coinMarketcapCoinsUrl);
    }

    getHeaders(): HttpHeaders {
        const headers = new HttpHeaders();
        headers.append("Accept", "application/json");
        return headers;
    }

    getToken(symbol: string): string {
        return symbol;
    }
}
