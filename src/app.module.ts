import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app/app.component";
import { MarketDataComponent } from "./app/market-data.component";
import { MarketDataService } from "./app/market-data.service";
import { KLineComponent } from "./app/kline.component";

import { appRouterModule } from "./app.routes";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        appRouterModule
    ],
    declarations: [
        AppComponent,
        MarketDataComponent,
        KLineComponent
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        MarketDataService
    ]
})
export class AppModule {}
