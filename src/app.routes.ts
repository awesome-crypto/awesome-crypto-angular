import { Routes, RouterModule } from "@angular/router";
import { MarketDataComponent } from "./app/market-data.component";
import { KLineComponent } from "./app/kline.component";

const routes: Routes = [
    {
        path: "",
        component: MarketDataComponent
    },
    {
        path: "marketData/:market",
        component: KLineComponent
    }
];

export const appRouterModule = RouterModule.forRoot(routes);
